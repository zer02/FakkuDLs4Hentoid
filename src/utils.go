package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"math/rand"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

// GetSpaceLimitedString returns a string adjusted to max length (available terminal space)
func GetSpaceLimitedString(inputString string, maxLength int, cutLeft bool) (limitedString string) {
	if len(inputString) < maxLength {
		return inputString
	} else if cutLeft {
		return "… " + string(inputString[len(inputString)-(maxLength-1):])
	} else {
		return string(inputString[0:maxLength-1]) + " …"
	}
}

// SplitStringN returns an array of strings from a string splitted after a certain length
func SplitStringN(s string, chunkSize int) []string {
	var chunks []string
	runes := []rune(s)
	if len(runes) == 0 {
		return []string{s}
	}

	for i := 0; i < len(runes); i += chunkSize {
		nn := i + chunkSize
		if nn > len(runes) {
			nn = len(runes)
		}
		chunks = append(chunks, string(runes[i:nn]))
	}
	return chunks
}

// FindDirectoriesWithoutSubdirs scans a path recursively looking for terminal folders
func FindDirectoriesWithoutSubdirs(path string, terminalFolders *[]string) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	dirCount := len(files)
	for _, file := range files {
		if !file.IsDir() {
			dirCount--
			if dirCount == 0 {
				*terminalFolders = append(*terminalFolders, path)
			}
		} else {
			FindDirectoriesWithoutSubdirs(filepath.Join(path, file.Name()), terminalFolders)
		}
	}
}

// GetDirectSearchString returns a string where all non-ascii and other unsafe URL chars have been replaced by dash
// Used by Fakku direct title and author search
func GetDirectSearchString(s string) string {
	safeString := strings.Replace(s, "'", "", -1) // handle apostrophe seperatly
	re := regexp.MustCompile("[^0-9A-Za-z]+")
	safeString = strings.ToLower(re.ReplaceAllString(safeString, "-"))

	return strings.Trim(safeString, "-")
}

// GetSafeSeachString returns a string where all unsafe URL chars have been removed
// Used by Fakku title search
func GetSafeSeachString(s string) string {
	re := regexp.MustCompile("[!~%()]+")
	safeString := re.ReplaceAllString(s, "")
	return strings.TrimSpace(safeString)
}

// GetFuzziedString returns a lowercased string where all chars outside of 0xFF range, whitespaces, dashes,
// any quote signs, slahes, points and colons have been omitted
// To handle titles that differ only on the last chars the function will append the original chars onto the return value
// Used by fuzzy compare title and artist
func GetFuzziedString(s string) string {
	// replace substitute chars with ordinary ones
	trueString := ReplaceSpecialChars(s)

	// cut out all crap
	re := regexp.MustCompile("[\"'`´„“”‘’*#,;()><\\/\\\\!?_.:\\-\\s]|[^\\x00-\\xFF]+")
	fuzzyString := strings.ToLower(re.ReplaceAllString(trueString, ""))
	if fullFuzzySearch {
		return fuzzyString
	}

	// append ending same chars no matter what they are, except when unicode control char
	runes := []rune(trueString)
	var stringBuilder strings.Builder
	for _, c := range runes {
		if c < 65024 || c > 65039 { // unicode control chars
			stringBuilder.WriteRune(c)
		}
	}
	runes = []rune(stringBuilder.String())
	lastChar := runes[len(runes)-1]
	fuzzyString += string(lastChar)
	for i := len(runes) - 2; i > 0; i-- {
		if runes[i] == lastChar {
			fuzzyString += string(lastChar)
		} else {
			break
		}
	}
	return fuzzyString
}

// ReplaceSpecialChars returns a string where certain special chars have been replaced by their ASCII counterpart
// Used by fuzzy search
func ReplaceSpecialChars(s string) string {
	var stringBuilder bytes.Buffer
	runes := []rune(s)
	for _, c := range runes {
		switch c {
		case '\uff1f': // ？
			stringBuilder.WriteRune('?')
		case '\u2049': // ⁉
			stringBuilder.WriteRune('!')
			stringBuilder.WriteRune('?')
		case '\u2026': // …
			stringBuilder.WriteRune('.')
			stringBuilder.WriteRune('.')
			stringBuilder.WriteRune('.')
		case '\u3008': //〈
			stringBuilder.WriteRune('<')
		case '\u3009': // 〉
			stringBuilder.WriteRune('>')
		case '\u00d7': // ×
			stringBuilder.WriteRune('x')
		case '\u00b9': // ¹
			stringBuilder.WriteRune('1')
		case '\u00b2': // ²
			stringBuilder.WriteRune('2')
		case '\u00b3': // ³
			stringBuilder.WriteRune('3')
		case '\u2733': // ✳
			stringBuilder.WriteRune('*')
		case '\u3002': // 。
			stringBuilder.WriteRune('.')
		default:
			stringBuilder.WriteRune(c)
		}
	}
	return stringBuilder.String()
}

// GetSafeSearchStringAlt returns a string where all unsafe URL chars have been removed or escaped
// Used by alternative title search
func GetSafeSearchStringAlt(s string) string {
	re := regexp.MustCompile("[#&]|[^\\x00-\\xFF]+")
	safeString := re.ReplaceAllString(s, " ")
	safeString = strings.Replace(safeString, " - ", " ", -1) // handle ':' that got turned to ' - '
	re = regexp.MustCompile("[\\s]+")
	safeString = re.ReplaceAllString(safeString, " ")
	var stringBuilder bytes.Buffer // need to unescape everything out of ascii range - see https://github.com/golang/go/issues/4385
	for _, c := range safeString {
		if c > 127 {
			stringBuilder.WriteString(url.QueryEscape(string(c)))
		} else {
			stringBuilder.WriteRune(c)
		}
	}
	safeString = strings.Replace(stringBuilder.String(), " ", "%20", -1)
	return safeString
}

// GetDirectSearchStringAlt returns an lowercased string where space have been replaced by underscore
// Used by alternative artist search
func GetDirectSearchStringAlt(s string) string {
	safeString := strings.Replace(strings.ToLower(s), " ", "_", -1)
	return safeString
}

// CaseInsensitiveContains checks whether a string contains a substring matching case insensitive
func CaseInsensitiveContains(source, searchString string) bool {
	return strings.Contains(strings.ToUpper(source), strings.ToUpper(searchString))
}

// SleepRandomByRange sleeps a random amount of milliseconds of the given range
// will not sleep if max is less than 100ms
func SleepRandomByRange(min, max int) {
	if max < 100 {
		return
	}
	SimpleLog("sleeping...")
	timeOut := make(chan bool, 1)
	if min == max {
		logInfo.Printf("sleeping %dms", min)
		go func() {
			time.Sleep(time.Duration(min) * time.Millisecond)
			timeOut <- true
		}()
	} else {
		duration := rand.Intn(max-min) + min
		logInfo.Printf("sleeping %dms", duration)
		go func() {
			time.Sleep(time.Duration(duration) * time.Millisecond)
			timeOut <- true
		}()
	}
	<-timeOut // wait for sleep to finish
	close(timeOut)
}

// FileOrDirExists checks whether a specific file or directory exists and if it can be accessed
func FileOrDirExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}
