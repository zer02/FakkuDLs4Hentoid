package main

import (
	"strings"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// ShowSimpleModal shows user a basic modal on top of current view
func ShowSimpleModal(title string, text string, width int, height int, wrap bool) {
	app.SetAfterDrawFunc(func(screen tcell.Screen) {
		// draw modal as simple box
		app.Lock()
		screenWidth, screenHeight := screen.Size()
		centerY := screenHeight / 2
		centerX := screenWidth / 2

		// draw top and bottom line
		for cx := centerX - (width / 2); cx < centerX+(width/2); cx++ {
			screen.SetContent(cx, centerY+(height/2), tview.GraphicsDbVertBar, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
			screen.SetContent(cx, centerY-(height/2)-1, tview.GraphicsDbVertBar, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
		}

		// draw vertical lines
		for cy := centerY - (height / 2); cy < centerY+(height/2); cy++ {
			screen.SetContent(centerX+(width/2), cy, tview.GraphicsDbHorBar, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
			screen.SetContent(centerX-(width/2)-1, cy, tview.GraphicsDbHorBar, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
		}

		// set corners
		screen.SetContent(centerX-(width/2)-1, centerY+(height/2), tview.GraphicsDbBottomLeftCorner, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
		screen.SetContent(centerX+(width/2), centerY+(height/2), tview.GraphicsDbBottomRightCorner, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
		screen.SetContent(centerX-(width/2)-1, centerY-(height/2)-1, tview.GraphicsDbTopLeftCorner, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))
		screen.SetContent(centerX+(width/2), centerY-(height/2)-1, tview.GraphicsDbTopRightCorner, nil, tcell.StyleDefault.Foreground(tcell.ColorWhite))

		// clear box area
		for cy := centerY - (height / 2); cy < centerY+(height/2); cy++ {
			for cx := centerX - (width / 2); cx < centerX+(width/2); cx++ {
				screen.SetContent(cx, cy, '\u2588', nil, tcell.StyleDefault.Foreground(tcell.ColorBlack))
			}
		}

		// write title if any
		if title != "" {
			tview.Print(screen, title, centerX-((width/2)-2), centerY-((height/2)+1), width-4, tview.AlignLeft, tcell.ColorWhite)
		}

		// write text if any
		if text != "" {
			var textLine []string
			if wrap {
				textLine = SplitStringN(text, width-2)
			} else {
				textLine = strings.Split(text, "\n")
			}
			for i, line := range textLine {
				if i > ((height / 2) + 1) {
					break
				}
				tview.Print(screen, line, centerX-((width/2)-1), centerY-(height/2)+1+i, width, tview.AlignLeft, tcell.ColorWhite)
			}
		}

		app.Unlock()
	})
}

// ShowModal prompts user a basic modal with a choice on top of current view
func ShowModal(title string, text string, width int, height int, yesFunc func(), noFunc func(), yesKey tcell.Key, noKey tcell.Key, wrap bool) {
	afterDrawHandler = app.GetAfterDrawFunc()
	inputCaptureHandler = app.GetInputCapture()

	ShowSimpleModal(title, text, width, height, wrap)

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == yesKey || event.Key() == noKey {
			app.SetAfterDrawFunc(nil)
			if afterDrawHandler != nil {
				app.SetAfterDrawFunc(afterDrawHandler)
			}
			app.SetInputCapture(nil)
			if inputCaptureHandler != nil {
				app.SetInputCapture(inputCaptureHandler)
			}
			app.Draw()

			if event.Key() == noKey {
				noFunc()
			} else if event.Key() == yesKey {
				yesFunc()
			}
		}
		return event
	})
}
