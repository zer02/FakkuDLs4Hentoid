package main

import (
	"flag"
	"os"
	"path/filepath"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

var app *tview.Application                                            // The tview base applicaion
var afterDrawHandler func(screen tcell.Screen)                        // afterDrawHandler in case we overwrite it
var inputCaptureHandler (func(event *tcell.EventKey) *tcell.EventKey) // inputCaptureHandler in case we overwrite it
var workingDirectory string                                           // The working directory of the app
var crawlDelayMin int                                                 // Minimum crawling delay
var crawlDelayMax int                                                 // Maximum crawling delay
var thumbnailWidth uint                                               // Width for all thumbnails
var fullFuzzySearch bool                                              // Indicates whether fuzzysearch should omit even the last character if outside of safe range
var noFuzzySearch bool                                                // Indicates whether fuzzysearch should be used or not
var noFakku bool                                                      // Indicates whether fakku shout be crawled or not

func main() {
	app = tview.NewApplication()

	// create main view elements
	logoBox := tview.NewFrame(tview.NewBox()).SetBorders(0, 0, 0, 0, 0, 0).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("███████╗ █████╗ ██╗  ██╗██╗  ██╗██╗   ██╗██╗", true, tview.AlignCenter, tcell.ColorMaroon).
		AddText("██╔════╝██╔══██╗██║ ██╔╝██║ ██╔╝██║   ██║██║", true, tview.AlignCenter, tcell.ColorMaroon).
		AddText("█████╗  ███████║█████╔╝ █████╔╝ ██║   ██║██║", true, tview.AlignCenter, tcell.ColorMaroon).
		AddText("██╔══╝  ██╔══██║██╔═██╗ ██╔═██╗ ██║   ██║╚═╝", true, tview.AlignCenter, tcell.ColorMaroon).
		AddText("██║     ██║  ██║██║  ██╗██║  ██╗╚██████╔╝██╗", true, tview.AlignCenter, tcell.ColorMaroon).
		AddText("╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝", true, tview.AlignCenter, tcell.ColorMaroon).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("Downloads 4 Hentoid v1.2.1", true, tview.AlignCenter, tcell.ColorGray).
		AddText("- by zer02 -", true, tview.AlignCenter, tcell.ColorGray).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("", true, tview.AlignCenter, tcell.ColorGray).
		AddText("Use the current directory?", true, tview.AlignCenter, tcell.ColorWhite).
		AddText("Press [red][ENTER[][white] to confirm", true, tview.AlignCenter, tcell.ColorWhite).
		AddText("Press [red][ESCAPE[][white] to exit", true, tview.AlignCenter, tcell.ColorWhite)

	// set flags
	mainModulePath, _ := os.Executable()
	var delayLow uint
	var delayHigh uint
	flag.StringVar(&workingDirectory, "dir", filepath.Dir(mainModulePath), "working directory path")
	flag.UintVar(&delayLow, "delayMin", 2500, "minimum fetch delay in milliseconds")
	flag.UintVar(&delayHigh, "delayMax", 5000, "maximum fetch delay in milliseconds")
	flag.UintVar(&thumbnailWidth, "thumbWidth", 350, "width for new thumbnails in pixel")
	flag.BoolVar(&fullFuzzySearch, "fullFuzzy", false, "fuzzy search will omit even the last character")
	flag.BoolVar(&noFuzzySearch, "noFuzzy", false, "disables fuzzy matching title and artist")
	flag.BoolVar(&noFakku, "noFakku", false, "don't use fakku for metadata fetching")

	statusBarMain := tview.NewTextView().SetTextColor(tcell.ColorGray).SetDynamicColors(false)
	statusBarMain.SetBorder(true)

	// build main view
	mainView := tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(logoBox, 0, 10, true).
		AddItem(statusBarMain, 3, 1, false)

	// build the pages
	pages := tview.NewPages()
	pages.AddAndSwitchToPage("main", mainView, true)
	pages.AddPage("working", WorkingView(), true, false)

	flag.Parse()
	if delayLow > delayHigh {
		delayLow = delayHigh
	}
	if thumbnailWidth < 50 {
		thumbnailWidth = 50
	}
	crawlDelayMin = int(delayLow)
	crawlDelayMax = int(delayHigh)

	// check whether we show welcome screen
	if filepath.Dir(mainModulePath) == workingDirectory {
		// get size of element after drawing and show current working dir
		app.SetAfterDrawFunc(func(screen tcell.Screen) {
			_, _, statusBarWidth, _ := statusBarMain.GetRect()
			statusBarMain.SetText(GetSpaceLimitedString(workingDirectory, statusBarWidth-2, true))
			app.SetAfterDrawFunc(nil)
		})

		// set hotkeys
		app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
			if event.Key() == tcell.KeyEsc {
				app.Stop()
			}
			if event.Key() == tcell.KeyEnter {
				pages.SwitchToPage("working")
				StartWorking(workingDirectory)
			}
			return event
		})
	} else { // start working directly
		pages.SwitchToPage("working")
		app.SetAfterDrawFunc(func(screen tcell.Screen) {
			StartWorking(workingDirectory)
			app.SetAfterDrawFunc(nil)
		})
	}

	// start app
	if err := app.SetRoot(pages, true).SetFocus(mainView).Run(); err != nil {
		panic(err)
	}
}
