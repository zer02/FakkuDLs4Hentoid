package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// Hmanga struct to hold basic data
type Hmanga struct {
	path     string
	basePath string
	artist   string
	title    string
	comic    string
	pages    int
	parody   string
	tags     []string
	images   []string
}

type logLevel int

const (
	logSuccess logLevel = 0
	logSkip    logLevel = 1
	logWarning logLevel = 2
	logFail    logLevel = 3

	fakkuDomain       string = "https://www.fakku.net"
	pandaChaikaDomain string = "https://panda.chaika.moe"
)

var logInfo *log.Logger                   // Logging normal stream
var logError *log.Logger                  // Logging critical problems
var bufferedLog *bufio.Writer             // An input that writes logging messages to file when buffer is full
var table *tview.Table                    // The main table view
var progressBar *tview.TextView           // A view element used as a simple progression bar
var progressStatus *tview.TextView        // A view element to show current progression
var progressStatusLog *tview.TextView     // A view element to show short status messages
var progressStatusFail *tview.TextView    // A view element to show count of failed work items
var progressStatusWarning *tview.TextView // A view element to show count of successfull work items with a warning
var progressStatusSkip *tview.TextView    // A view element to show count of skipped work items
var progressStatusSuccess *tview.TextView // A view element to show count of failed work items
var potentialDirectories []string         // An array storing all work items
var httpClient *http.Client               // A pre-initializable http.Client
var tableWidth int                        // The available width for printing
var countSuccess int                      // A counter for successfull work items
var countSkip int                         // A counter for skipped work items
var countWarning int                      // A counter for successfull work items with a warning
var countFail int                         // A counter for failed work items
var lastProgress float32                  // The current progression on the progression bar

// WorkingView returns the view content
func WorkingView() (content tview.Primitive) {
	// create working view elements
	table = tview.NewTable().SetSelectable(false, false)
	progressBar = tview.NewTextView().SetTextColor(tcell.ColorGray).SetDynamicColors(false).SetWrap(false)
	progressBar.SetBorder(true)
	progressStatus = tview.NewTextView().SetTextColor(tcell.ColorGray).SetDynamicColors(false).SetWrap(false).SetTextAlign(tview.AlignCenter).SetChangedFunc(func() { app.Draw() }) // redraw all if change occurs here
	progressStatus.SetBorder(true)
	progressStatusLog = tview.NewTextView().SetTextColor(tcell.ColorGray).SetDynamicColors(false).SetWrap(false).SetChangedFunc(func() { app.Draw() }) // redraw all if change occurs here
	progressStatusLog.SetBorder(true)
	progressStatusFail = tview.NewTextView().SetTextColor(tcell.ColorRed).SetDynamicColors(false).SetWrap(false).SetTextAlign(tview.AlignCenter)
	progressStatusFail.SetBorder(true)
	progressStatusWarning = tview.NewTextView().SetTextColor(tcell.ColorYellow).SetDynamicColors(false).SetWrap(false).SetTextAlign(tview.AlignCenter)
	progressStatusWarning.SetBorder(true)
	progressStatusSkip = tview.NewTextView().SetTextColor(tcell.ColorGrey).SetDynamicColors(false).SetWrap(false).SetTextAlign(tview.AlignCenter)
	progressStatusSkip.SetBorder(true)
	progressStatusSuccess = tview.NewTextView().SetTextColor(tcell.ColorGreen).SetDynamicColors(false).SetWrap(false).SetTextAlign(tview.AlignCenter)
	progressStatusSuccess.SetBorder(true)
	statusBar := tview.NewFlex().SetDirection(tview.FlexColumn).
		AddItem(progressBar, 50+2, 8, false).
		AddItem(progressStatus, 11+2, 4, false).
		AddItem(progressStatusLog, 0, 1, false).
		AddItem(progressStatusFail, 4+2, 1, false).
		AddItem(progressStatusWarning, 4+2, 1, false).
		AddItem(progressStatusSkip, 4+2, 1, false).
		AddItem(progressStatusSuccess, 5+2, 1, false)

	// build working view
	workingView := tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(table, 0, 10, true).
		AddItem(statusBar, 3, 1, false)

	// initialize
	httpClient = &http.Client{Timeout: 8 * time.Second} // default http.Client does not have a time out
	rand.Seed(time.Now().Unix())

	return workingView
}

// StartWorking starts the work load
func StartWorking(directory string) {
	if !FileOrDirExists(workingDirectory) {
		log.Fatal("Working directory does not exist or is unaccessable")
	}
	logFile, err := os.Create(filepath.Join(workingDirectory, "FakkuDLs4HentoidLog.txt"))
	defer logFile.Close()
	if err != nil {
		log.Fatal("Could not create log file: " + err.Error())
	}
	bufferedLog = bufio.NewWriter(logFile)
	logInfo = log.New(bufferedLog, "", log.LstdFlags) // writing info log to buffer first so we keep IO low
	logError = log.New(io.MultiWriter(logFile, os.Stderr), "ERROR: ", log.LstdFlags|log.Lshortfile)

	// assuming we are on WorkingView now - get size of element after drawing
	app.SetAfterDrawFunc(func(screen tcell.Screen) {
		afterDrawHandler = app.GetAfterDrawFunc()
		_, _, tableWidth, _ = table.GetRect()
		app.SetAfterDrawFunc(nil)
	})

	// set input handler
	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyEsc {
			ShowModal("Exit Application?", "Press [red][ENTER[][white] to exit\nPress [red][ESC[][white] to return", 30, 4, func() {
				logInfo.Printf("user exited")
				bufferedLog.Flush()
				app.Stop()
			}, func() {}, tcell.KeyEnter, tcell.KeyEsc, false)
		}
		return event
	})
	app.Draw()

	indexDirectory(directory)
}

// indexDirectory gathers all directories and sub-directories looking for valid HManga folders to parse
func indexDirectory(directory string) {
	ShowSimpleModal("Indexing Directory", "Please wait...", 30, 4, false)
	app.Draw()

	logInfo.Printf("Indexing directory: %s", directory)
	potentialDirectories = make([]string, 0)
	FindDirectoriesWithoutSubdirs(directory, &potentialDirectories)
	fmt.Fprintf(progressStatus, "%s", "0/")
	fmt.Fprintf(progressStatus, "%d", len(potentialDirectories))

	// remove modal
	app.SetAfterDrawFunc(nil)
	app.Draw()

	app.SetFocus(table)
	processDirectories()
}

// processDirectories works through all work items creating thumbnails and contentV2.json
func processDirectories() {
	logInfo.Printf("found %d potential folders", len(potentialDirectories))
	for _, dir := range potentialDirectories {
		SimpleLog("processing folder...")
		logInfo.Printf("reading folder: %s", dir)
		baseDir := filepath.Base(dir)
		artist, title, comic := parseArtistAndTitle(baseDir)
		if artist == "" || title == "" {
			logEntryToTable(dir, logSkip, "not a valid directory name")
			continue
		}
		logInfo.Printf("parsed folder name to: artist '%s', title '%s', magazine '%s'", artist, title, comic)
		imageFiles, manualLookup := readPages(dir)
		if imageFiles == nil {
			logEntryToTable(dir, logSkip, "already done")
			continue
		}
		logInfo.Printf("found %d images", len(imageFiles))
		hmanga := Hmanga{
			artist:   artist,
			title:    title,
			pages:    len(imageFiles),
			comic:    comic,
			path:     dir,
			basePath: baseDir,
			images:   imageFiles,
		}
		hmangaOnline, err := gatherOnlineInformation(hmanga, manualLookup)
		if hmangaOnline.tags == nil {
			logEntryToTable(dir, logFail, err)
			continue
		}
		hmangaOnline.pages = hmanga.pages
		hmangaOnline.path = hmanga.path
		hmangaOnline.basePath = hmanga.basePath
		hmangaOnline.images = hmanga.images
		logInfo.Printf("found match online: '%s': %d tags, parody: '%s'", hmangaOnline.title, len(hmangaOnline.tags), hmangaOnline.parody)
		saveErr := SaveHmangaToJSON(hmangaOnline)
		if saveErr != nil {
			logEntryToTable(dir, logFail, "could not write JSON!")
			continue
		}
		thumb := generateThumbnail(hmangaOnline)
		if !thumb {
			err = "thumbnail creation failed"
		}
		if err != "" {
			logEntryToTable(dir, logWarning, err)
		} else {
			logEntryToTable(dir, logSuccess, "")
		}
	}
	SimpleLog("finished")
	logInfo.Printf("%d successfull, %d skipped, %d with warning, %d failed", countSuccess, countSkip, countWarning, countFail)
	logInfo.Printf("all work done")
	app.Draw()
	bufferedLog.Flush()
	rowCount := table.GetRowCount()
	if rowCount > 1 {
		app.SetFocus(table)
		table.SetSelectable(true, false)
		table.Select(rowCount-2, 0)
	}
}

// parseArtistAndTitle returns artist, title and comic parsed from a Hmanga dir if valid
// directory must be named like '[Artist] Title'
// multiple artists are supported but only the first one will be parsed: '[Artist1, Artist2] Title'
// alternative artist names are supported but only the true artist name will be parsed: '[ArtistAlt (Artist)] Title'
// Comic in title will be parsed if present: '[Artist] Title (COMIC Nr)'
// all other tags in brackets will be omitted
func parseArtistAndTitle(dirName string) (string, string, string) {
	if dirName[0] != '[' || !strings.ContainsRune(dirName, ']') {
		return "", "", ""
	}
	indexArtistEnd := strings.Index(dirName, "]")
	artist := dirName[1:indexArtistEnd]
	if strings.ContainsRune(artist, ',') { // handle multiple artists - always use first one
		artist = artist[0:strings.Index(artist, ",")]
	}
	if strings.ContainsRune(artist, '(') && strings.ContainsRune(artist, ')') { // handle alternative artist names - always use name in brackets
		leftIndex := strings.Index(artist, "(") + 1
		rightIndex := strings.Index(artist, ")")
		if leftIndex > rightIndex {
			return "", "", ""
		}
		artist = artist[leftIndex:rightIndex]
	}
	title := dirName[indexArtistEnd+2:]
	comic := ""
	if CaseInsensitiveContains(title, " (COMIC ") { // parse COMIC from title
		upperTitle := strings.ToUpper(title)
		comic = title[strings.Index(upperTitle, "(COMIC")+7:]
		comic = "Comic " + comic[0:len(comic)-1]
		if strings.ContainsRune(comic, ')') {
			comic = comic[0:strings.Index(comic, ")")]
		}
		title = title[0 : strings.Index(upperTitle, "(COMIC")-1]
	}
	firstIndex := strings.IndexAny(title, "()[]{}") // remove other possible tags
	if firstIndex > -1 {
		title = strings.TrimSpace(title[0:firstIndex])
	}

	return artist, title, comic
}

// readPages returns an array of string containing names of all image files and URL if manual content is set
// returns nil if contentV2.json is already present, empty string if manual lookup content is not set
func readPages(path string) ([]string, string) {
	files, _ := ioutil.ReadDir(path)
	imageFiles := make([]string, 0, len(files))
	manualLookup := ""
	for _, file := range files {
		if file.Name() == "contentV2.json" {
			return nil, ""
		} else if file.Name() == "content.txt" {
			contentFile, _ := os.Open(filepath.Join(path, file.Name()))
			scanner := bufio.NewScanner(contentFile)
			if scanner.Scan() {
				manualLookup = scanner.Text()
			}
			contentFile.Close()
			continue
		}
		extension := file.Name()
		extension = extension[len(extension)-4:]
		if extension == ".png" || extension == ".jpg" || extension == ".jpeg" {
			if !CaseInsensitiveContains(file.Name(), "thumb") {
				imageFiles = append(imageFiles, file.Name())
			}
		}
	}
	return imageFiles, manualLookup
}

// gatherOnlineInformation returns matched Hmanga and an error string for a Hmanga
// if tags are nil a fatal error happened or the Hmanga hasn't been found
func gatherOnlineInformation(hmanga Hmanga, manualLookup string) (Hmanga, string) {
	var matchedHmanga Hmanga
	var err error
	var link string
	if manualLookup != "" {
		link = strings.Replace(manualLookup, " ", "%20", -1)
		logInfo.Printf("trying to access title manually: fetching '%s'", link)
		if strings.Contains(link, "fakku.net") {
			matchedHmanga, err = AccessHMangaOnline(link, hmanga, true)
			if len(matchedHmanga.tags) > 0 {
				if err != nil {
					return matchedHmanga, err.Error()
				}
				return matchedHmanga, ""
			}
		} else if strings.Contains(link, "panda.chaika.moe") {
			matchedHmanga, err = SearchHMangaOnlineAlternative(link, hmanga, true)
			if len(matchedHmanga.tags) > 0 {
				if err != nil {
					return matchedHmanga, err.Error()
				}
				return matchedHmanga, ""
			}
		}
		return Hmanga{}, "not found"
	}
	link = fakkuDomain + "/hentai/" + GetDirectSearchString(hmanga.title) + "-english"
	if !noFakku {
		// try accessing title directly
		logInfo.Printf("trying to access title directly: fetching '%s'", link)
		matchedHmanga, err = AccessHMangaOnline(link, hmanga, false)
		if len(matchedHmanga.tags) > 0 {
			if err != nil {
				return matchedHmanga, err.Error()
			}
			return matchedHmanga, ""
		}
		// try searching for title
		link = fakkuDomain + "/search/" + url.PathEscape(GetSafeSeachString(hmanga.title))
		logInfo.Printf("trying to search for title: fetching '%s'", link)
		matchedHmanga, err = SearchHMangaOnline(link, hmanga)
		if len(matchedHmanga.tags) > 0 {
			if err != nil {
				return matchedHmanga, err.Error()
			}
			return matchedHmanga, ""
		}
		// try searching for artist
		link = fakkuDomain + "/artists/" + GetDirectSearchString(hmanga.artist)
		logInfo.Printf("trying to search for artist: fetching '%s'", link)
		matchedHmanga, err = SearchHMangaOnline(link, hmanga)
		if len(matchedHmanga.tags) > 0 {
			if err != nil {
				return matchedHmanga, err.Error()
			}
			return matchedHmanga, ""
		}
	}
	// try searching on alternative host
	link = pandaChaikaDomain + "/jsearch?g&title=" + GetSafeSearchStringAlt(hmanga.title) + "&tags=artist:" + GetDirectSearchStringAlt(hmanga.artist)
	logInfo.Printf("trying to search for title and artist on alternative source: fetching '%s'", link)
	matchedHmanga, err = SearchHMangaOnlineAlternative(link, hmanga, false)
	if len(matchedHmanga.tags) > 0 {
		if err != nil {
			return matchedHmanga, err.Error()
		}
		if !noFakku {
			return matchedHmanga, "not found on fakku..."
		}
		return matchedHmanga, ""
	}
	if noFakku {
		// try searching for title only on alternative host
		link = pandaChaikaDomain + "/jsearch?g&title=" + GetSafeSearchStringAlt(hmanga.title)
		logInfo.Printf("trying to search for title on alternative source: fetching '%s'", link)
		matchedHmanga, err = SearchHMangaOnlineAlternative(link, hmanga, false)
		if len(matchedHmanga.tags) > 0 {
			if err != nil {
				return matchedHmanga, err.Error()
			}
			return matchedHmanga, ""
		}
		// try searching for artist only on alternative host
		link = pandaChaikaDomain + "/jsearch?g&tags=artist:" + GetDirectSearchStringAlt(hmanga.artist)
		logInfo.Printf("trying to search for artist on alternative source: fetching '%s'", link)
		matchedHmanga, err = SearchHMangaOnlineAlternative(link, hmanga, false)
		if len(matchedHmanga.tags) > 0 {
			if err != nil {
				return matchedHmanga, err.Error()
			}
			return matchedHmanga, ""
		}
	}
	return Hmanga{}, "not found"
}

// generateThumbnail creates a thumbnail from the first image of a Hmanga using CatmullRom for resampling
func generateThumbnail(hmanga Hmanga) bool {
	thumbPath := filepath.Join(hmanga.path, "thumb.jpg")
	if FileOrDirExists(thumbPath) {
		logInfo.Printf("skipping creation of thumbnail: already present")
		return true
	}
	SimpleLog("creating thumbnail...")
	err := CreateThumbnail(filepath.Join(hmanga.path, hmanga.images[0]), thumbPath, int(thumbnailWidth))
	if err != nil {
		logInfo.Printf("error while creating thumbnail: '%s'", err.Error())
		return false
	}
	return true
}

// logEntryToTable will print all progression to view
func logEntryToTable(dir string, logLevel logLevel, message string) {
	rowCount := table.GetRowCount()
	logMessage := "[grey]" + string('\u2514') + string('\u2500')
	switch logLevel {
	case logSuccess:
		logMessage += " [green]success"
		countSuccess++
		progressStatusSuccess.Clear()
		fmt.Fprintf(progressStatusSuccess, "%d", countSuccess)
		logInfo.Printf("successfully added: '%s'", dir)
	case logSkip:
		logMessage += " [grey]skipped[white] - " + message
		countSkip++
		progressStatusSkip.Clear()
		fmt.Fprintf(progressStatusSkip, "%d", countSkip)
		logInfo.Printf("skipping '%s': %s", dir, message)
	case logWarning:
		logMessage += " [yellow]warning[white] - " + message
		countWarning++
		progressStatusWarning.Clear()
		fmt.Fprintf(progressStatusWarning, "%d", countWarning)
		logInfo.Printf("added '%s' with warning: %s", dir, message)
	case logFail:
		logMessage += " [red]failed[white] - " + message
		countFail++
		progressStatusFail.Clear()
		fmt.Fprintf(progressStatusFail, "%d", countFail)
		logInfo.Printf("failed to add '%s': %s", dir, message)
	}
	table.SetCell(rowCount, 0, tview.NewTableCell(GetSpaceLimitedString(tview.Escape(dir), tableWidth-2, true)))
	table.SetCell(rowCount+1, 0, tview.NewTableCell(logMessage).SetSelectable(false))

	// calculate and print progression
	currentCount := (rowCount + 2) / 2
	newProgression := (float32(currentCount) / float32(len(potentialDirectories)) * 100.0) - lastProgress
	for i := newProgression; i >= 2.0; i -= 2.0 {
		fmt.Fprintf(progressBar, "%s", "\u2588")
		lastProgress += 2.0
	}

	progressStatus.Clear()
	fmt.Fprintf(progressStatus, "%d", currentCount)
	fmt.Fprintf(progressStatus, "%s", "/")
	fmt.Fprintf(progressStatus, "%d", len(potentialDirectories))

	// write out log
	bufferedLog.Flush()
}

// SimpleLog prints some status update to log view
func SimpleLog(message string) {
	progressStatusLog.Clear()
	fmt.Fprintf(progressStatusLog, "%s", message)
}
