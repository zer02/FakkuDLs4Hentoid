package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

type pandaChaikaEntry struct {
	Filecount int      `json:"filecount"`
	Source    string   `json:"source"`
	Tags      []string `json:"tags"`
	Title     string   `json:"title"`
}

// AccessHMangaOnline fetches an URL and tries to access a HManga directly
// returning matches Hmanga and an error object
func AccessHMangaOnline(link string, hmanga Hmanga, noCheck bool) (Hmanga, error) {
	page, err := parseURL(link)
	if err != nil {
		logInfo.Printf("error while accessing webpage: '%s'", err.Error())
		return Hmanga{}, err
	}

	// get div to right side of screen which holds all meta data
	var contentRight *html.Node
	getContentRight := func(n *html.Node) bool {
		if n.Type == html.ElementNode && n.Data == "div" && n.FirstChild != nil {
			if hasNodeAttribute(n.Attr, "class", "content-right") { // <div class="content-right"><div class="content-name"> ...
				contentRight = n
				return true
			}
		}
		return false
	}
	success := forEachNodeCheck(page, getContentRight)
	if !success {
		logInfo.Printf("error while parsing '%s': content-right not found", link)
		return Hmanga{}, err
	}
	var result Hmanga
	result.tags = make([]string, 0, 15)
	result.pages = 1

	// extract all tags from tag div
	// <div class="row-right tags ellipsis-white"><a href="/tags/XXX">XXX</a> ...
	extractTags := func(n *html.Node) {
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if c.Type == html.ElementNode {
				if c.Data != "a" {
					break
				} else if c.FirstChild != nil {
					result.tags = append(result.tags, c.FirstChild.Data)
				}
			}
		}
	}

	// get all metadata from content-right div
	getMetadata := func(n *html.Node) bool {
		if n.Type == html.ElementNode {
			if n.Data == "div" && n.FirstChild != nil {
				if hasNodeAttribute(n.Attr, "class", "content-name") { // <div class="content-name"><h1>XXX</h1></div>
					if n.FirstChild.NextSibling != nil && n.FirstChild.NextSibling.Type == html.ElementNode && n.FirstChild.NextSibling.Data == "h1" {
						if n.FirstChild.NextSibling.FirstChild != nil {
							result.title = n.FirstChild.NextSibling.FirstChild.Data // some bug in golang.org/x/net/html - see https://github.com/golang/go/issues/15996
						}
					}
				} else if hasNodeAttribute(n.Attr, "class", "row-right") { // <div class="row-right"><a href="/artists/XXX">XXX</a></div>
					if n.FirstChild.Type == html.ElementNode && n.FirstChild.Data == "a" {
						for _, attr := range n.FirstChild.Attr {
							if attr.Key == "href" {
								if strings.HasPrefix(attr.Val, "/artists/") { // could be multiple artists but we only fetch the first one
									result.artist = n.FirstChild.FirstChild.Data
								} else if strings.HasPrefix(attr.Val, "/series/") {
									result.parody = n.FirstChild.FirstChild.Data
								} else if strings.HasPrefix(attr.Val, "/magazines/") {
									result.comic = n.FirstChild.FirstChild.Data
								}
							}
						}
					}
					// pages
					if n.FirstChild.Type == html.TextNode { // <div class="row-right">XX pages</div>
						if strings.Contains(n.FirstChild.Data, "pages") {
							if result.pages == 1 {
								pageParts := strings.Split(n.FirstChild.Data, " ")
								result.pages, err = strconv.Atoi(pageParts[0])
								if err != nil {
									logInfo.Printf("error while parsing '%s': could not read pages: '%s'", link, n.FirstChild.Data)
									result.pages = -1
								}
							}
						}
					}

				} else if hasNodeAttributeLoose(n.Attr, "class", "row-right tags") {
					extractTags(n)
					return true // tags are last
				}
			}
		}
		return false
	}
	forEachNodeCheck(contentRight, getMetadata)

	found := true
	if !noCheck {
		found = false
		found, err = compareHMangas(hmanga, result)
	}
	if found {
		logInfo.Printf("match found on %s", link)
		return result, err
	}
	logInfo.Printf("search unsuccessful on %s", link)
	return Hmanga{}, err
}

// SearchHMangaOnline fetches an URL and tries to search for a matching HManga in a list of search entries
// returning matched Hmanga and an error object
func SearchHMangaOnline(link string, hmanga Hmanga) (Hmanga, error) {
	page, err := parseURL(link)
	if err != nil {
		logInfo.Printf("error while accessing webpage: '%s'", err.Error())
		return Hmanga{}, err
	}

	// get first comic, skipping games and anime
	// <div class="content-row border-radius content-comic">...
	comicNodes := make([]*html.Node, 0, 10)
	var firstComicNode *html.Node
	getFirstComic := func(n *html.Node) bool {
		if n.Type == html.ElementNode && n.Data == "div" && n.FirstChild != nil {
			if hasNodeAttributeLoose(n.Attr, "class", "content-comic") {
				firstComicNode = n
				return true
			}
		}
		return false
	}
	success := forEachNodeCheck(page, getFirstComic)
	if !success {
		logInfo.Printf("no comic found on '%s'", link)
		return Hmanga{}, fmt.Errorf("no comic found on parsed page")
	}

	paginationPages := 1
	// if any find pagination and count available pages
	// <div class="pagination-results">...<a class="pagination-page" href="/XXX/page/2" title="Page 2">2</a>...
	getPagination := func(n *html.Node) bool {
		if hasNodeAttributeLoose(n.Attr, "class", "pagination-results") {
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				if c.Type == html.ElementNode && c.Data == "a" {
					if hasNodeAttribute(c.Attr, "class", "pagination-page") {
						paginationPages++
					}
				}
			}
			return true
		}
		return false
	}

	// find all comics which are siblings inside the same div
	// <div class="content-row border-radius content-comic">...</div><div class="content-row border-radius content-comic">..</div>...
	getRemainingComicsAndPages := func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "div" && n.FirstChild != nil {
			if hasNodeAttributeLoose(n.Attr, "class", "content-comic") {
				comicNodes = append(comicNodes, n)
			} else if hasNodeAttributeLoose(n.Attr, "class", "pagination") {
				forEachNodeCheck(n, getPagination)
			}
		}
	}
	forEachSiblingNodeCheck(firstComicNode, getRemainingComicsAndPages)
	if paginationPages > 1 {
		logInfo.Printf("found %d pages", paginationPages)
	}

	result, err := indexSearchResults(comicNodes, hmanga)
	if result.title != "" {
		logInfo.Printf("match found on '%s'", link)
		return result, err
	}

	// find all comics without pagination as we already got that
	getRemainingComics := func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "div" && n.FirstChild != nil {
			if hasNodeAttributeLoose(n.Attr, "class", "content-comic") {
				comicNodes = append(comicNodes, n)
			}
		}
	}

	// loop over all pages searching for a match
	logInfo.Printf("not found on page 1")
	for i := 2; i <= paginationPages; i++ {
		pageURL := link + "/page/" + strconv.Itoa(i)
		logInfo.Printf("fetching next page: '%s'", pageURL)
		page, err = parseURL(pageURL)
		if err != nil {
			logInfo.Printf("error while accessing webpage: '%s'", err.Error())
			return Hmanga{}, err
		}
		firstComicNode = nil
		comicNodes = make([]*html.Node, 0, 10)
		if !forEachNodeCheck(page, getFirstComic) {
			logInfo.Printf("no comic found on '%s'", pageURL)
			continue
		}
		forEachSiblingNodeCheck(firstComicNode, getRemainingComics)
		result, err := indexSearchResults(comicNodes, hmanga)
		if result.title != "" {
			logInfo.Printf("match found on '%s'", pageURL)
			if err != nil {
				logInfo.Printf("warning: '%s'", err.Error())
			}
			return result, err
		}
		logInfo.Printf("not found on page %d", i)
	}

	logInfo.Printf("search unsuccessful on %s", link)
	return Hmanga{}, err
}

// indexSearchResults enumerates all Hmangas on a page of search results and tries to find a match to given Hmanga
// returns a Hmanga is match has been found
func indexSearchResults(comicNodes []*html.Node, hmanga Hmanga) (Hmanga, error) {
	var result Hmanga
	result.pages = 1
	var err error

	extractTags := func(n *html.Node) {
		result.tags = make([]string, 0, 15)
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if c.Type == html.ElementNode {
				if c.Data != "a" {
					break
				} else if c.FirstChild != nil {
					result.tags = append(result.tags, c.FirstChild.Data)
				}
			}
		}
	}

	getMetadata := func(n *html.Node) bool {
		if n.Type == html.ElementNode {
			if n.Data == "div" && n.FirstChild != nil {
				if hasNodeAttribute(n.Attr, "class", "content-meta") { // <div class="content-meta"><h2><a class="content-title">Title</a></h2>...
					if n.FirstChild.NextSibling != nil && n.FirstChild.NextSibling.Type == html.ElementNode && n.FirstChild.NextSibling.Data == "h2" { //
						if n.FirstChild.NextSibling.FirstChild != nil && n.FirstChild.NextSibling.FirstChild.Type == html.ElementNode && n.FirstChild.NextSibling.FirstChild.Data == "a" {
							titleLink := n.FirstChild.NextSibling.FirstChild
							if titleLink.FirstChild != nil {
								result.title = titleLink.FirstChild.Data // some bug in golang.org/x/net/html - see https://github.com/golang/go/issues/15996
							}
						}
					}
				} else if hasNodeAttribute(n.Attr, "class", "row-left") { // <div class="row-left">Pages</div><div class="row-right">X</div>
					if n.FirstChild.Type == html.TextNode && strings.Contains(n.FirstChild.Data, "Pages") {
						if n.NextSibling != nil && n.NextSibling.NextSibling != nil { // <div class="row-right">20</div>
							rowRight := n.NextSibling.NextSibling
							if result.pages == 1 && rowRight.FirstChild != nil {
								result.pages, err = strconv.Atoi(rowRight.FirstChild.Data)
								if err != nil {
									logInfo.Printf("error while parsing: could not read pages: '%s'", rowRight.FirstChild.Data)
									result.pages = -1
								}
							}
						}
					}
				} else if hasNodeAttribute(n.Attr, "class", "row-right") { // <div class="row-right"><a href="/artists/XXX">XXX</a></div>
					if n.FirstChild.Type == html.ElementNode && n.FirstChild.Data == "a" {
						for _, attr := range n.FirstChild.Attr {
							if attr.Key == "href" {
								if strings.HasPrefix(attr.Val, "/artists/") { // could be multiple artists but we only fetch the first one
									result.artist = n.FirstChild.FirstChild.Data
								} else if strings.HasPrefix(attr.Val, "/magazines/") {
									result.comic = n.FirstChild.FirstChild.Data
								}
							}
						}
					}
				} else if hasNodeAttributeLoose(n.Attr, "class", "row-right tags") {
					extractTags(n)
					var found bool // tags are last
					found, err = compareHMangas(hmanga, result)
					if found {
						return true
					}
					result.pages = 1
				}
			}
		}
		return false
	}
	// get metadata for every comic and compare them
	for _, comicNode := range comicNodes {
		if forEachNodeCheck(comicNode, getMetadata) {
			return result, err
		}
	}

	return Hmanga{}, fmt.Errorf("comic not found on parsed page")
}

// SearchHMangaOnlineAlternative fetches an URL and uses the panda.chaika api to search for a matching HManga
// returning matched Hmanga and an error object
func SearchHMangaOnlineAlternative(link string, hmanga Hmanga, noCheck bool) (Hmanga, error) {
	jsonResult := make([]pandaChaikaEntry, 0)
	err := parseURLToJSON(link, &jsonResult)
	if err != nil || len(jsonResult) < 1 {
		return Hmanga{}, err
	}
	// can return a collection of results
	logInfo.Printf("parsed %d results", len(jsonResult))
	for i := 0; i < len(jsonResult); i++ {
		result := Hmanga{
			title: jsonResult[i].Title,
			pages: jsonResult[i].Filecount,
			tags:  make([]string, 0, 15),
		}
		for _, tag := range jsonResult[i].Tags {
			if strings.HasPrefix(tag, "artist:") {
				result.artist = tag[7:]
			} else if strings.HasPrefix(tag, "parody:") {
				result.parody = tag[7:]
			} else if strings.ContainsRune(tag, ':') { // some information like language and publisher is inside tags so we skip them
				continue
			} else {
				result.tags = append(result.tags, tag)
			}
		}

		found := true
		if !noCheck {
			found = false
			found, err = compareHMangasAlternative(hmanga, result)
		}
		if found {
			logInfo.Printf("match found on '%s'", link)
			if err != nil {
				logInfo.Printf("warning: '%s'", err.Error())
			}
			result.artist = hmanga.artist // do not use artist from panda chaika as their format is crappy
			return result, err
		}
	}

	logInfo.Printf("search unsuccessful on %s", link)
	return Hmanga{}, err
}

// compareHMangas compares two Hmanga objects returning specific errors for different mismatch types
// returns false if title or artist mismatch
func compareHMangas(original Hmanga, parsed Hmanga) (bool, error) {
	var err error
	if !strings.EqualFold(original.title, parsed.title) {
		if !noFuzzySearch && (GetFuzziedString(original.title) == GetFuzziedString(parsed.title)) {
			err = fmt.Errorf("title fuzzy match: '%s' <> '%s'", original.title, parsed.title)
		} else {
			return false, fmt.Errorf("title mismatch: '%s' <> '%s'", original.title, parsed.title)
		}
	}
	if !strings.EqualFold(original.artist, parsed.artist) {
		if !noFuzzySearch && (GetFuzziedString(original.artist) == GetFuzziedString(parsed.artist)) {
			if err == nil {
				err = fmt.Errorf("artist fuzzy match: '%s' <> '%s'", original.artist, parsed.artist)
			}
		} else {
			return false, fmt.Errorf("artist mismatch: '%s' <> '%s'", original.artist, parsed.artist)
		}
	}
	if original.comic != "" && !strings.EqualFold(original.comic, parsed.comic) {
		if err == nil {
			err = fmt.Errorf("magazine mismatch: %s <> %s", original.comic, parsed.comic)
		}
	}
	if original.pages != parsed.pages {
		if err == nil {
			err = fmt.Errorf("page mismatch: %d <> %d", original.pages, parsed.pages)
		}
	}
	return true, err
}

// compareHMangas compares two Hmanga objects returning specific errors for different mismatch types
// returns false if title mismatch
func compareHMangasAlternative(original Hmanga, parsed Hmanga) (bool, error) {
	if !strings.EqualFold(original.title, parsed.title) {
		if !noFuzzySearch && (GetFuzziedString(original.title) == GetFuzziedString(parsed.title)) {
			return true, fmt.Errorf("title fuzzy match: '%s' <> '%s'", original.title, parsed.title)
		}
		return false, fmt.Errorf("title mismatch: '%s' <> '%s'", original.title, parsed.title)
	}
	if original.pages != parsed.pages {
		return true, fmt.Errorf("page mismatch: %d <> %d", original.pages, parsed.pages)
	}
	return true, nil
}

// parseURL will fetch and parse a given URL into a html.Node object
// after successfull fetching function will sleep a random delay to follow crawl delays
func parseURL(link string) (*html.Node, error) {
	SimpleLog("fetching...")
	resp, err := httpClient.Get(link)
	if err != nil {
		return nil, err
	}
	SleepRandomByRange(crawlDelayMin, crawlDelayMax)
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("fetching %s failed: %d", link, resp.StatusCode)
	}
	SimpleLog("parsing...")
	doc, err := html.Parse(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, fmt.Errorf("parsing %s failed: %s", link, err.Error())
	}
	return doc, err
}

// parseURLToJSON will fetch and parse a given URL resulting in a JSON into a target interface
// after successfull fetching function will sleep a random delay to follow crawl delays if noFakku switch is set
func parseURLToJSON(link string, target interface{}) error {
	SimpleLog("fetching alternative...")
	resp, err := httpClient.Get(link)
	if err != nil {
		return err
	}
	if noFakku {
		SleepRandomByRange(crawlDelayMin, crawlDelayMax)
	}
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return fmt.Errorf("fetching %s failed: %d", link, resp.StatusCode)
	}
	defer resp.Body.Close()
	SimpleLog("parsing alternative...")
	return json.NewDecoder(resp.Body).Decode(target)
}

// forEachNode iterates over all nodes calling a function on every visit
// if function returns true iteration will stop
func forEachNodeCheck(n *html.Node, handler func(n *html.Node) bool) bool {
	if handler(n) {
		return true
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if forEachNodeCheck(c, handler) {
			return true
		}
	}
	return false
}

// forEachSiblingNodeCheck iterates over all sibling nodes calling a function on every visit
func forEachSiblingNodeCheck(n *html.Node, handler func(n *html.Node)) {
	handler(n)
	for s := n.NextSibling; s != nil; s = s.NextSibling {
		handler(s)
	}
}

// hasNodeAttribute iterates over all attributes comparing given key and val returning true if matched
func hasNodeAttribute(attributes []html.Attribute, key string, val string) bool {
	for _, attr := range attributes {
		if attr.Key == key && attr.Val == val {
			return true
		}
	}
	return false
}

// hasNodeAttributeLoose iterates over all attributes comparing given key and val returning true if matched partially
func hasNodeAttributeLoose(attributes []html.Attribute, key string, val string) bool {
	for _, attr := range attributes {
		if attr.Key == key && strings.Contains(attr.Val, val) {
			return true
		}
	}
	return false
}
