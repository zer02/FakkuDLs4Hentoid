package main

import (
	"encoding/json"
	"os"
	"path/filepath"
	"time"
)

// contentV2 struct to hold all data as in Hentoid 1.2.5
type contentV2 struct {
	Attributes    attribute   `json:"attributes"`
	CoverImageURL string      `json:"coverImageUrl"`
	DownloadDate  int64       `json:"downloadDate"`
	ImageFiles    []imageFile `json:"imageFiles"`
	QtyPages      int         `json:"qtyPages"`
	Site          string      `json:"site"`
	Status        string      `json:"status"`
	Title         string      `json:"title"`
	UploadDate    int64       `json:"uploadDate"`
	URL           string      `json:"url"`
}

type attribute struct {
	Tag      []attributeEntry `json:"TAG"`
	Serie    []attributeEntry `json:"SERIE"`
	Language []attributeEntry `json:"LANGUAGE"`
	Artist   []attributeEntry `json:"ARTIST"`
}

type attributeEntry struct {
	Name string `json:"name"`
	Type string `json:"type"`
	URL  string `json:"url"`
}

type imageFile struct {
	Name   string `json:"name"`
	Order  int    `json:"order"`
	Status string `json:"status"`
	URL    string `json:"url"`
}

// SaveHmangaToJSON serializes a Hmanga struct into a valid contentV2 JSON Hentoid object and writes it to file
func SaveHmangaToJSON(hmanga Hmanga) error {
	fakeURLBase := "https://FakkuDLs4Hentoid/" + hmanga.basePath + "/" // all 'URL' must start with a valid web-prefix
	content := contentV2{
		CoverImageURL: fakeURLBase + "/thumb.jpg",
		DownloadDate:  time.Now().UnixNano() / 1000000, // Unix Milliseconds
		ImageFiles:    make([]imageFile, 0, hmanga.pages),
		QtyPages:      hmanga.pages,
		Site:          "FAKKU",
		Status:        "DOWNLOADED",
		Title:         hmanga.title,
		UploadDate:    0,
		URL:           fakeURLBase,
	}

	for i, image := range hmanga.images {
		iFile := imageFile{
			Name:   image,
			Order:  i + 1,
			Status: "DOWNLOADED",
			URL:    fakeURLBase + image,
		}
		content.ImageFiles = append(content.ImageFiles, iFile)
	}

	tagAttribute := make([]attributeEntry, 0, len(hmanga.tags))
	for _, tag := range hmanga.tags {
		entry := attributeEntry{
			Name: tag,
			Type: "TAG",
			URL:  "/tags/" + tag,
		}
		tagAttribute = append(tagAttribute, entry)
	}
	if hmanga.comic != "" { // add magazine as tag as there is no dedicated space for it
		tagEntryComic := attributeEntry{
			Name: hmanga.comic,
			Type: "TAG",
			URL:  "/magazines/" + GetDirectSearchString(hmanga.comic),
		}
		tagAttribute = append(tagAttribute, tagEntryComic)
	}

	serieAttribute := make([]attributeEntry, 0, 1)
	if hmanga.parody != "" {
		serieEntry := attributeEntry{
			Name: hmanga.parody,
			Type: "SERIE",
			URL:  "/series/" + GetDirectSearchString(hmanga.parody),
		}
		serieAttribute = append(serieAttribute, serieEntry)
	}

	langAttribute := make([]attributeEntry, 0, 1)
	langEntry := attributeEntry{
		Name: "english",
		Type: "LANGUAGE",
		URL:  "/hentai/english",
	}
	langAttribute = append(langAttribute, langEntry)

	artistAttribute := make([]attributeEntry, 0, 1)
	artistEntry := attributeEntry{
		Name: hmanga.artist,
		Type: "ARTIST",
		URL:  "/artists/" + GetDirectSearchString(hmanga.artist),
	}
	artistAttribute = append(artistAttribute, artistEntry)

	attributes := attribute{
		Tag:      tagAttribute,
		Language: langAttribute,
		Artist:   artistAttribute,
	}
	if len(serieAttribute) > 0 {
		attributes.Serie = serieAttribute
	}
	content.Attributes = attributes

	contentJSON, err := json.Marshal(content)
	if err != nil {
		logInfo.Printf("error while marshaling JSON: '%s'", err.Error())
		return err
	}
	jsonFile, err := os.Create(filepath.Join(hmanga.path, "contentV2.json"))
	if err != nil {
		logInfo.Printf("error while writing JSON: '%s'", err.Error())
		return err
	}
	jsonFile.Write(contentJSON)
	jsonFile.Close()

	return nil
}
