# FakkuDLs4Hentoid

A metadata/tags crawler to import FAKKU releases to [Hentoid](https://github.com/avluis/Hentoid) written in Go

[Builds are available here for windows, linux and mac](https://gitlab.com/zer02/FakkuDLs4Hentoid/tags)

FakkuDLs4Hentoid will automatically fetch online metadata, create thumbnails and write everything to json-files so you can import all of your *legally owned* digital downloads from FAKKU (books, comics, dōjinshi) into Hentoid. Online sources for fetching are fakku.net and panda.chaika.moe as fallback.

![welcome screen on ubuntu](https://i.imgur.com/o8ALGUP.png)

![working view on windows](https://i.imgur.com/LhZczNM.png)

## Prerequisites

For a download to be recognized as such it has to be unpacked into a folder without any subdirectories. The accepted folder naming convention is `[Artist] Title`, magazine is also supported, must be in brackets after the title starting with 'COMIC': `[Artist] Title (COMIC Magazine)`. Artist name can contain alternative names but only the last one in brackets will be used, multiple artists should be delimited by a comma however only the first one will be parsed. Unicode, umlauts and emojis in directory name are handled and used for fetching. Folder must contain at least one image of the following format `*.png`, `*.jpg` or `*.gif`. The first image inside the folder will be used to create the thumbnail.

Here are some examples assuming working directory for FakkuDLs4Hentoid is `F:\MyFakkuSubscription\` 

Valid examples
```
F:\MyFakkuSubscription\Downloads\[ArtistName] Title
F:\MyFakkuSubscription\Downloads\[ArtistName] Title (COMIC Magazine)
F:\MyFakkuSubscription\Downloads\[OldArtistName (ArtistName)] Title (COMIC Magazine)
F:\MyFakkuSubscription\Downloads\[ArtistNameA, ArtistNameB, ArtistNameC] Title (COMIC Magazine)
F:\MyFakkuSubscription\Downloads\Books\[ArtistName] Title (COMIC Magazine) (some stuff)
F:\MyFakkuSubscription\Downloads\Books\[ArtistName] Title [some stuff]

F:\MyFakkuSubscription\Downloads\BestDōjinshi\even better ones\[zer02] How's my code? (COMIC Gitlab 2018-04)
```
Invalid examples
```
F:\MyFakkuSubscription\Downloads\ArtistName Title
F:\MyFakkuSubscription\Downloads\[ArtistName] [Title in any brackets]
F:\MyFakkuSubscription\Downloads\[ArtistName] Title with any (brackets)
F:\MyFakkuSubscription\Downloads\[ArtistName] Title\some folder
```

Every folder inside the working directory has to be read and writable otherwise indexing will fail.

Fuzzy search will normally preserve the last character (multiple occurrences are handled) so we can distinguish between chapters of a series that go from 1 to 2 to 3 repetitions of said character with each chapter. This however makes it necessary that **the last character** matches the online information of the title perfectly. Some chars like `?` or `>` can not be used for file/folder names. In that case you'll have to use a special unicode substitute as last character:

```
online  local
?   ->  ？
!?  ->  ⁉
>   ->  〉
<   -> 〈
:   ->  ' - ' (whitespace dash whitespace)
*   ->  ✳ or ' ' (whitespace)
```

You can however disable this behavior with the `fullFuzzy` switch which will also omit the last character if outside of 'safe' range. Doing so is not recommended on the first run because you could end up with tags for the wrong chapter. Better manually fix the last character on these folders. If you still would like to use `fullFuzzy` then keep an eye on magazine mismatches which would indicate matching wrong chapters.

## Usage

[Download the latest release of FakkuDLs4Hentoid](https://gitlab.com/zer02/FakkuDLs4Hentoid/tags), copy it into the directory containing your extracted digital downloads and execute it. Confirm the working directory with `Enter` and wait while all metadata is fetched. You can always exit the application with `Escape` and confirming action with `Enter`.

You can also provide the working directory and alter various settings using following command line arguments:

```
--help                  Prints a short help text
--dir PATH              Use the specified working directory path, must be enclosed by quotation 
                        marks if whitespaces are present (default is current directory)
--thumbWidth NUMBER     Use specified width in pixel for new thumbnails 
                        (default is 350, minimum is 50)
--delayMin NUMBER       Use specified delay in milliseconds as minimum sleeping duration between 
                        crawling actions (default is 2500, must be less or equal to delayMax)
--delayMax NUMBER       Use specified delay in milliseconds as maximum sleeping duration between 
                        crawling actions (default is 5000)
--fullFuzzy             Do not preserve the last character when using fuzzy search. Use this when 
                        search fails because of title mismatch on the last character. Not 
                        recommended on the first run
--noFuzzy               Do not use fuzzy string compare for title and artist, use this in the 
                        very rare event of getting a false-positive match
--noFakku               Do not use fakku for fetching metadata, use alternative host exclusively
```

Example:
```
FakkuDLs4Hentoid.exe -dir="F:\MyFakkuSubscription" -thumbWidth=400 -delayMin=3500 -delayMax=8000 -noFuzzy
```
Be aware that a low delay could result in your IP getting temporary banned from Fakku and panda.chaika. Delay can be set to zero by setting `delayHigh` to 0 (not recommended).

You can also manually instruct FakkuDLs4Hentoid to use a direct link for fetching a certain folder, in case if the automatic search returns a false-positive or it can't be found online automatically. Simply create a file called `content.txt` inside the folder and paste the link into the first line of the text file. Be aware however that there will be no checks after fetching. For Fakku the link should directly lead to the title's page. If this title can not be found on Fakku (deleted from Fakku) then use panda.chaika. For that you'll have to build an API search string that returns your desired title as the first search result element. Artist is handled as a tag on panda.chaika, should be lowercase only and spaces replaced by underscore `_`. The title can contain spaces and unicode chars but `&` and `#` should be removed. The link must be properly escaped.

Some valid examples assuming we are trying to find a H-Manga called "How's my Code?" written by "Zero Two":

```
https://www.fakku.net/hentai/hows-my-code-english
https://www.fakku.net/hentai/hows-my-code-english-1234567890
https://panda.chaika.moe/jsearch?g&title=How%27s%20my%20Code
https://panda.chaika.moe/jsearch?g&title=How%27s%20my%20Code&tags=artist:zero_two
```

After processing has finished you'll find a file called `FakkuDLs4HentoidLog.txt` inside the working directory that will contain all actions and errors during execution. It will be overwritten on next startup.

## Importing to Hentoid

After processing every valid folder should contain a `thumb.jpg` and a `contentV2.json`. You can copy all folders to `\Hentoid\Downloads\` on your mobile device and import them via Hentoid -> Settings -> Prefs -> Select Downloads Folder -> *select the Hentoid folder*.

Depending on the amount and size of the new library this could take up to a minute. Afterwards press the refresh-button in the main menu and all your Fakku Downloads will show up in Hentoid.

Unfortunately the current version of Hentoid (1.2.4) does not support recursive folder search so you'll have to copy all processed Fakku comics/books/dōjinshi into the top directory, subdirectories will be ignored.

## Dependencies

* [tview](https://github.com/rivo/tview) - Rich interactive widgets for terminal-based UIs written in Go

## Contributing

Feel free to open an issue or create a pull request at any time

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Known problems

* if the last character of the title is different to online data then even fuzzy search will fail, fix folder name or use --fullFuzzy switch for this
* if title ends with a character that can't be used in file names like `?` you'll have to use an unicode substitute character or use --fullFuzzy switch, see prerequisites
* title can not contain normal brackets, just remove them and use fuzzy search (default), if title ends with `)` you'll have to use --fullFuzzy switch
* some extremely special titles can not be matched on alternative host, use Fakku for fetching or instruct FakkuDLs4Hentoid to use a manual direct link, see prerequisites
* artist name will not be fuzzy matched on alternative host, make sure it's correct
* some unicode characters are not shown correctly on windows
* under very strange circumstances fuzzy search could return tags for the wrong chapter of a series (not happened yet)
* creating a thumbnail from an animated gif will fail
* fakku and panda.chaika might temporary ban your IP if you crawl too much or set delay too low

## Version History

* v1.2.1 (2018-04-28)
  * Warnings are now continuously handled
  * Fixed an issue where the replacement of `:` ( - ) wouldn't be used correctly
  * Added an unicode substitute for `*`
  * Added an unicode substitute for `.` although not neccessary
  * Added more logging
* v1.2.0 (2018-04-24)
  * Added option to manually provide a direct fetching web link per folder
  * Added search for title and artist alone on alternative host if --noFakku switch is used
  * Fixed an issue where artist name had underscore instead of spaces when fetching from alternative host
  * Fixed an issue where pages would not be parsed correctly if entry had a single page
  * Small improvements
* v1.1.0 (2018-04-22)
  * Improved fuzzy search.
  * Added --fullFuzzy switch.
  * Online metadata will now be used for artist and title too.
  * Increased default minimum crawling delay from 2000ms to 2500ms.
  * Increased timeout for http.Get from 5 to 8 seconds.
  * Sleeping will now use a goroutine.
  * Fixed various small mistakes.
* v1.0.0 (2018-04-21)
  * Initial release.
